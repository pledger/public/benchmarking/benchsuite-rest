#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from flask_restx import Namespace, fields, Resource
from flask import g


from benchsuite.rest.apiv3.providers import provider_model_short
from benchsuite.rest.apiv3.workloads import workload_short_model

import uuid

from benchsuite.rest.db import get_db

api = Namespace('schedules', description='Schedules operations')


wild = fields.Wildcard(fields.String)

nested_object = api.model('NestedObject', {
    '*': wild
})


scheduling_hints = api.model('SchedulingHints', {
    'interval': fields.Nested(nested_object, skip_none=True),
    '*': wild
})


schedule_model = api.model('Schedule', {
    'id': fields.String,
    'name': fields.String,
    'description': fields.String,
    'properties': fields.Nested(nested_object, skip_none=True),
    'env': fields.Nested(nested_object, skip_none=True),
    'tags': fields.List(fields.String),
    'tests': fields.List(fields.String),
    'active': fields.Boolean,
    'metadata': fields.Nested(nested_object, skip_none=True),
    'benchsuite_additional_opts': fields.List(fields.String),
    'service_types': fields.List(fields.Nested(nested_object, skip_none=True)),
    'docker_additional_opts': fields.Nested(nested_object, skip_none=True),
    'workload_parameters': fields.Nested(nested_object, skip_none=True),
    'grants': fields.Nested(nested_object, skip_none=True),
    'provider_id': fields.String,
    'provider': fields.Nested(provider_model_short, skip_none=True),
    'workloads': fields.List(fields.Nested(workload_short_model, skip_none=True)),
    "username" : fields.String,
    "provider_config_secret" : fields.String,
    "reports-scope" : fields.String,
    "scheduling_hints": fields.Nested(scheduling_hints, skip_none=True),
    # this will not work when marshalling input model (api.payload...)
    "created_time": fields.DateTime,
    "updated_time": fields.DateTime
})

schedule_model_short = api.model('ScheduleShort', {
    'id': fields.String,
    'name': fields.String,
    'description': fields.String,
})

def _getProvider(provider_id, requestorName=None):
    if provider_id:
        return get_db().pm.get_provider(provider_id, requestorName=requestorName)
    return None

def _getWorkload(workload_id, requestorName=None):
    if workload_id:
        return get_db().wm.get_workload(workload_id, requestorName=requestorName)
    return None

def _addProviderAndWorkloads(schedule, requestorName=None):
    if schedule["provider_id"]:
        schedule["provider"] = _getProvider(schedule["provider_id"], requestorName=requestorName)
    if schedule["tests"]:
        schedule["workloads"] = []
        for id in schedule["tests"]:
            schedule["workloads"].append(_getWorkload(id, requestorName=requestorName))
    return schedule

@api.route('/')
class SchedulesList(Resource):
    

    @api.marshal_with(schedule_model, skip_none=True, as_list=True, code=200, description='Returns the list of all schedules')
    def get(self):
        schedules = get_db().sm.list_schedules(requestorName=g.benchUser, embedProvider=True, embedWorkloads=True)
#            for s in schedules:
#                _addProviderAndWorkloads(s, requestorName=g.benchUser)
        return schedules

    @api.expect(schedule_model)
    @api.marshal_with(schedule_model, code=201, description='Add a new schedule')
    def post(self):
        api.payload["id"] = str(uuid.uuid4())
        return get_db().sm.add_schedule(api.payload, requestorName=g.benchUser), 201

@api.route('/user_actions')
class AllowedSchedulesActions(Resource):
    @api.marshal_with(nested_object, as_list=True, code=200, description='Returns the list of available actions on schedules')
    def get(self):
        actions = get_db().sm.get_allowed_actions(None, requestorName=g.benchUser)
        return actions

@api.route('/<string:schedule_id>')
@api.param('schedule_id', 'The id of the schedule')
class Schedule(Resource):

    @api.marshal_with(schedule_model, skip_none=True, as_list=False, code=200, description='Returns the given schedule')
    @api.response(404, description='No such schedule')
    def get(self, schedule_id):
        schedule = get_db().sm.get_schedule(schedule_id, requestorName=g.benchUser, embedWorkloads=True, embedProvider=True)
        if schedule==None:
            api.abort(404, "Schedule '{}' doesn't exist".format(schedule_id))
        else:
#                _addProviderAndWorkloads(schedule, requestorName=g.benchUser)
            return schedule

    @api.expect(schedule_model)
    @api.marshal_with(schedule_model, code=200, description='Update an existing schedule')
    def put(self, schedule_id):
        if not get_db().sm.schedule_exists(schedule_id):
            api.abort(404, "Schedule '{}' doesn't exist".format(schedule_id))

        api.payload["id"] = schedule_id
        ret = get_db().sm.update_schedule(api.payload, requestorName=g.benchUser)
        if ret and ret=="unauthorized":
            api.abort(401, "Not authorized to updated schedule {}".format(schedule_id))
        else:
            return '', 204

    @api.response(204, description='Deletes a schedule')
    @api.response(404, description='No such schedule')
    def delete(self, schedule_id):
        ret = get_db().sm.delete_schedule(schedule_id, requestorName=g.benchUser)
        if ret and ret=="unauthorized":
            api.abort(401, "Not authorized to remove schedule {}".format(schedule_id))
        elif ret and ret.deleted_count>0:
            return '', 204
        else:
            api.abort(404, "Schedule {} doesn't exist".format(schedule_id))
                
                
@api.route('/<string:schedule_id>/user_actions')
@api.param('schedule_id', 'The id of the schedule')
class AllowedScheduleActions(Resource):
    @api.marshal_with(nested_object, as_list=True, code=200, description='Returns the list of available actions on the schedule')
    @api.response(404, description='No such schedule')
    def get(self, schedule_id):
        actions = get_db().sm.get_allowed_actions(schedule_id, requestorName=g.benchUser)
        return actions
        