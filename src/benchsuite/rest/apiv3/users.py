#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from flask_restx import Namespace, fields, Resource
from benchsuite.rest.db import get_um
from flask import g
import uuid

api = Namespace('users', description='Users operations')

user_model = api.model('User', {
    'firstName': fields.String,
    'lastName': fields.String,
    'email': fields.String,
    'username': fields.String
})

organization_model = api.model('Organization', {
    'id': fields.String,
    'name': fields.String,
    'web': fields.String
})

user_scopes = api.model('UserScopes', {
    'username': fields.String,
    'scopes': fields.List(fields.String)
})

@api.route('/')
class UserList(Resource):

    @api.marshal_with(user_model, as_list=True, code=200, description='Returns the list of all users')
    def get(self):
        get_um().get_users()

    @api.expect(user_model)
    @api.marshal_with(user_model, code=201, description='Add a new user')
    def post(self):
        api.payload["id"] = str(uuid.uuid4())
        return get_um().add_user(api.payload), 201

@api.route('/<string:user_id>')
@api.param('user_id', 'The id of the user')
class User(Resource):

    @api.marshal_with(user_model, as_list=False, code=200, description='Returns the given user')
    @api.response(404, description='No such user')
    def get(self, user_id):
        user = get_um().get_user(user_id)
        if user==None:
            api.abort(404, "User '{}' doesn't exist".format(user_id))
        else:
            return user

    @api.response(204, description='Deletes a user')
    @api.response(404, description='No such user')
    def delete(self, user_id):
        ret = get_um().delete_user(user_id)
        if ret.deleted_count>0:
            return '', 204
        else:
            api.abort(404, "User {} doesn't exist".format(user_id))


    @api.expect(user_model)
    @api.marshal_with(user_model, code=200, description='Update an existing user')
    def put(self, user_id):
        if not get_um().user_exists(user_id):
            api.abort(404, "User '{}' doesn't exist".format(user_id))
        ret = get_um().update_user(api.payload)
        return '', 204
        

@api.route('/<string:username>/scopes')
@api.param('username', 'The id of the user')
class OrganizationUsers(Resource):

    @api.marshal_with(user_scopes, as_list=True, code=200, description='Return scopes this user has access to')
    @api.response(404, description='No such user')
    def get(self, username):
        if not get_um().user_exists(username):
            api.abort(404, "User '{}' doesn't exist".format(username))

        out = {"username":username, "scopes":[]}

        out["scopes"].append("usr:"+username)

        groups = get_um().get_user_groups(username)
        if groups:
            for gr in groups:
                out["scopes"].append("org:"+gr["name"])

        if g.config.always_include_public_scope:
            out["scopes"].append("org:public")

        return out
