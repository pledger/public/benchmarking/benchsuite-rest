#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from benchsuite.rest.apiv1 import register_error_handlers
from flask_restx import Namespace, fields, Resource, abort
from flask import g

from benchsuite.core.db.configuration_db import BenchsuiteConfigurationDB

import uuid

from benchsuite.rest.db import get_db

api = Namespace('providers', description='Providers operations')


@api.errorhandler
def default_error_handler(error):
    '''Default error handler'''
    return {'message': str(error)}, getattr(error, 'code', 500)



wild = fields.Wildcard(fields.String, skip_none=True)


nested_object = api.model('NestedObject', {
    '*': wild
})

service_properties_model = api.model("ServiceProperties", {
    'post_create_script': fields.Nested(nested_object, skip_none=True),
    'vm_user': fields.Nested(nested_object, skip_none=True),
    'vm_password': fields.Nested(nested_object, skip_none=True)
})

provider_model = api.model('Provider', {
    "id": fields.String,
    "name": fields.String,
    "driver": fields.String,
    "description": fields.String,
    'new_vm': fields.Nested(nested_object, skip_none=True),
    'grants': fields.Nested(nested_object, skip_none=True),
    'metadata': fields.Nested(nested_object, skip_none=True),
    'service_properties': fields.Nested(service_properties_model),
    '*': wild
})

provider_model_short = api.model('ProviderShort', {
    "name" : fields.String,
    "driver": fields.String,
    "description" : fields.String
})

image_model = api.model('Image', {
  '*': wild
})

size_model = api.model('Size', {
  '*': wild
})

param_descr_type_model = api.model('ParamDescrType', {
    'type': fields.String,
    'values': fields.List(fields.String),
    '*': wild
})
param_descr_model = api.model('ParamDescr', {
    'type': fields.Nested(param_descr_type_model, skip_none=True),
    'label': fields.String,
    'name': fields.String,
    'optional': fields.Boolean,
})

param_no_name_model = api.model('ParamNoName', {
    'label': fields.String,
    'value': fields.String,
    'unit': fields.String,
    'metadata': fields.Nested(nested_object, skip_none=True)
})

provider_type_model = api.model('ProviderType', {
    'name': fields.String,
    'label': fields.String,
    'connection_params_descr': fields.Nested(param_descr_model, skip_none=True),
})

param_model = api.clone('Param', param_no_name_model, {
    'name': fields.String
})

service_configurations_model = api.model('ServiceConfiguration', {
    'name': fields.String,
    'params': fields.List(fields.Nested(param_model, skip_none=True))
})


def prepare_provider_for_marshalling(provider):
    res = {k:v for k, v in provider.items() if v is not None}
    return res


@api.route('/')
class ProvidersList(Resource):

    @api.marshal_with(provider_model, skip_none=True, as_list=True, code=200, description='Returns the list of all providers')
    def get(self):
        return [prepare_provider_for_marshalling(p) for p in get_db().pm.list_providers(requestorName=g.benchUser)]

    @api.expect(provider_model)
    @api.marshal_with(provider_model, code=201, description='Add a new provider')
    def post(self):
        api.payload["id"] = str(uuid.uuid4())
        return get_db().pm.add_provider(api.payload, requestorName=g.benchUser), 201


@api.route('/user_actions')
class AllowedProvidersActions(Resource):
    @api.marshal_with(image_model, as_list=True, code=200, description='Returns the list of available actions on providers')
    def get(self):
        return get_db().pm.get_allowed_actions(None, requestorName=g.benchUser)


@api.route('/types')
class ProviderTypes(Resource):
    @api.marshal_with(provider_type_model, as_list=True, code=200, description='Returns the list of available provider types')
    def get(self):
        types = get_db().pm.list_provider_types()
        return [{
            'name': t.name,
            'label': t.label,
            'connection_params_descr': t.describe_connection_params()} for t in types]


@api.route('/<string:provider_id>')
@api.param('provider_id', 'The id of the provider')
class Provider(Resource):

    @api.marshal_with(provider_model, as_list=False, skip_none=False, code=200, description='Returns the given provider')
    @api.response(404, description='No such provider')
    def get(self, provider_id):
        provider = get_db().pm.get_provider(provider_id, requestorName=g.benchUser)
        if provider==None:
            api.abort(404, "Provider '{}' doesn't exist".format(provider_id))
        else:
            return prepare_provider_for_marshalling(provider)

    @api.response(204, description='Deletes a provider')
    @api.response(404, description='No such provider')
    def delete(self, provider_id):
        ret = get_db().pm.delete_provider(provider_id, requestorName=g.benchUser)
        if ret and ret=="unauthorized":
            api.abort(401, "Not authorized to remove provider {}".format(provider_id))
        elif ret and ret.deleted_count>0:
            return '', 204
        else:
            api.abort(404, "Provider {} doesn't exist".format(provider_id))

    @api.expect(provider_model)
    @api.marshal_with(provider_model, code=200, description='Update an existing provider')
    def put(self, provider_id):
        if not get_db().pm.provider_exists(provider_id):
            api.abort(404, "Provider '{}' doesn't exist".format(provider_id))
        api.payload["id"] = provider_id
        ret = get_db().update_provider(api.payload, requestorName=g.benchUser)
        if ret and ret=="unauthorized":
            api.abort(401, "Not authorized to updated provider {}".format(provider_id))
        else:
            return '', 204

    @api.expect(provider_model)
    @api.marshal_with(provider_model, code=200, description='Update an existing provider')
    def patch(self, provider_id):
        if not get_db().pm.provider_exists(provider_id):
            api.abort(404, "Provider '{}' doesn't exist".format(provider_id))
        api.payload["id"] = provider_id
        ret = get_db().pm.patch_provider(api.payload, requestorName=g.benchUser)
        if ret and ret=="unauthorized":
            api.abort(401, "Not authorized to updated provider {}".format(provider_id))
        else:
            return '', 204


@api.route('/<string:provider_id>/check')
@api.param('provider_id', 'The id of the provider')
class ProviderCheckConnection(Resource):

    @api.response(404, description='No such provider')
    @api.response(204, description='Connection successful')
    @api.response(500, description='Error during connection')
    def get(self, provider_id):
        provider = get_db().pm.load_provider_from_db(provider_id, requestorName=g.benchUser)
        if provider is None:
            abort(404, f'Provider "{provider_id}" does not exist')
        provider.check_connection()
        return '', 204


@DeprecationWarning
@api.route('/<string:provider_id>/vm_images')
@api.param('provider_id', 'The id of the provider')
class ProviderImages(Resource):

    @api.marshal_with(image_model, as_list=True, code=200, description='Returns the list of images available at provider')
    @api.response(404, description='No such provider')
    def get(self, provider_id):
        provider = get_db().pm.get_provider(provider_id, requestorName=g.benchUser)
        if provider is None:
            abort(404, f'Provider "{provider_id}" does not exist')
        images = get_db().pm.list_images(provider_id, requestorName=g.benchUser)
        if images is None:
            abort(404, f'Unable to get images for provider {provider_id}')
        return images


@DeprecationWarning
@api.route('/<string:provider_id>/vm_sizes')
@api.param('provider_id', 'The id of the provider')
class ProviderSizes(Resource):

    @api.marshal_with(size_model, as_list=True, code=200, description='Returns the list of sizes available at provider')
    @api.response(404, description='No such provider')
    def get(self, provider_id):
        provider = get_db().pm.get_provider(provider_id, requestorName=g.benchUser)
        if provider is None:
            abort(404, f'Provider "{provider_id}" does not exist')
        sizes = get_db().pm.list_sizes(provider_id, requestorName=g.benchUser)
        if sizes is  None:
            abort(404, f'Unable to get sizes for provider {provider_id}')
        return sizes


@api.route('/<string:provider_id>/vm/params')
@api.param('provider_id', 'The id of the provider')
class VMParams(Resource):

    @api.marshal_with(param_descr_model, as_list=True, skip_none=True, code=200, description='Returns the description of service parameters')
    @api.response(404, description='No such provider')
    def get(self, provider_id):
        provider = get_db().load_provider_from_db(provider_id, requestorName=g.benchUser)
        if provider is None:
            abort(404, f'Provider "{provider_id}" does not exist')
        params = provider.describe_service_params()
        if params is None:
            abort(404, f'Unable to get service parameters for provider {provider_id}')
        return params


@api.route('/<string:provider_id>/vm/images')
@api.param('provider_id', 'The id of the provider')
class VMImages(Resource):

    @api.marshal_with(param_no_name_model, as_list=True, skip_none=True, code=200, description='Returns the list of images available in the provider')
    @api.response(404, description='No such provider')
    def get(self, provider_id):
        provider = get_db().pm.load_provider_from_db(provider_id, requestorName=g.benchUser)
        if provider is None:
            abort(404, f'Provider "{provider_id}" does not exist')
        values = provider.list_provider_param_values('image')
        if values is None:
            abort(404, f'Unable to get images for provider {provider_id}')
        return values


@api.route('/<string:provider_id>/vm/flavours')
@api.param('provider_id', 'The id of the provider')
class VMFlavours(Resource):

    @api.marshal_with(service_configurations_model, as_list=True, skip_none=True, code=200, description='Returns the VM flavours defined in the provider')
    @api.response(404, description='No such provider')
    def get(self, provider_id):
        provider = get_db().pm.load_provider_from_db(provider_id, requestorName=g.benchUser)
        if provider is None:
            abort(404, f'Provider "{provider_id}" does not exist')
        flavours = provider.list_service_configurations()
        if flavours is None:
            abort(404, f'Unable to get flavours for provider {provider_id}')
        return flavours


@api.route('/<string:provider_id>/user_actions')
@api.param('provider_id', 'The id of the provider')
class AllowedProviderActions(Resource):
    @api.marshal_with(image_model, as_list=True, code=200, description='Returns the list of available actions on the provider')
    @api.response(404, description='No such provider')
    def get(self, provider_id):
        actions = get_db().pm.get_allowed_actions(provider_id, requestorName=g.benchUser)
        return actions

