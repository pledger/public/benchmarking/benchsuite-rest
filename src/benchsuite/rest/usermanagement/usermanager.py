#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
from .keycloak import KeycloakClient

logger = logging.getLogger(__name__)


class UserManager:

    def __init__(self, **kwargs):
        self.oidc_client = KeycloakClient(**kwargs)
        self.dao = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        return exc_type is None
        
    def get_user(self, username):
        return self.oidc_client.get_user(username)

    def get_user_groups(self, username):
        return self.oidc_client.get_user_groups(username)
        
    def get_users(self):
        return self.oidc_client.get_users()
        
    def user_exists(self, username):
        return self.oidc_client.user_exists(username)


