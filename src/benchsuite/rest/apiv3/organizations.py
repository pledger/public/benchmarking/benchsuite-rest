#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from flask_restx import Namespace, fields, Resource

import uuid

from benchsuite.rest.db import get_db

api = Namespace('organizations', description='Organizations operations')

organization_model = api.model('Organization', {
    'id': fields.String,
    'name': fields.String,
    'web': fields.String
})

user_model = api.model('User', {
    'id': fields.String,
    'familyName': fields.String,
    'givenName': fields.String,
    'organizations': fields.List(fields.String)
})

@api.route('/')
class OrganizationList(Resource):

    @api.marshal_with(organization_model, as_list=True, code=200, description='Returns the list of all organizations')
    def get(self):
        return get_db().um.list_organizations()

    @api.expect(organization_model)
    @api.marshal_with(organization_model, code=201, description='Add a new organization')
    def post(self):
        api.payload["id"] = str(uuid.uuid4())
        return get_db().um.add_organization(api.payload), 201

@api.route('/<string:organization_id>')
@api.param('organization_id', 'The id of the organization')
class Organization(Resource):

    @api.marshal_with(organization_model, as_list=False, code=200, description='Returns the given organization')
    @api.response(404, description='No such organization')
    def get(self, organization_id):
        organization = get_db().um.get_organization(organization_id)
        if organization==None:
            api.abort(404, "Organization '{}' doesn't exist".format(organization_id))
        else:
            return organization

    @api.response(204, description='Deletes an organization')
    @api.response(404, description='No such organization')
    def delete(self, organization_id):
        ret = get_db().um.delete_organization(organization_id)
        if ret.deleted_count>0:
            return '', 204
        else:
            api.abort(404, "Organization {} doesn't exist".format(organization_id))

    @api.expect(organization_model)
    @api.marshal_with(organization_model, code=200, description='Update an existing organization')
    def put(self, organization_id):
        if not get_db().um.organization_exists(organization_id):
            api.abort(404, "Organization '{}' doesn't exist".format(api.payload["id"]))
        ret = get_db().um.update_organization(api.payload)
        return '', 204

@api.route('/<string:organization_id>/users')
@api.param('organization_id', 'The id of the organization')
class OrganizationUsers(Resource):

    @api.marshal_with(user_model, as_list=True, code=200, description='Return users in this organization')
    @api.response(404, description='No such organization')
    def get(self, organization_id):
        if not get_db().um.organization_exists(organization_id):
            api.abort(404, "Organization '{}' doesn't exist".format(organization_id))
        return get_db().um.list_organization_users(organization_id)

    @api.expect(user_model)
    @api.marshal_with(user_model, code=201, description='Add a user to this organization')
    def post(self, organization_id):
        if not get_db().um.organization_exists(organization_id):
            api.abort(404, "Organization '{}' doesn't exist".format(organization_id))
        user_id = api.payload["id"]
        return get_db().um.add_organization_membership(organization_id, user_id), 201

 
@api.route('/<string:organization_id>/users/<string:user_id>')
@api.param('organization_id', 'The id of the organization')
class OrganizationUser(Resource):

    @api.response(204, description='Deletes a user from this organization')
    @api.response(404, description='No such user in this organization')
    def delete(self, organization_id, user_id):
        if not get_db().um.organization_exists(organization_id):
            api.abort(404, "Organization '{}' doesn't exist".format(organization_id))
        if not get_db().um.user_exists(user_id):
            api.abort(404, "User '{}' doesn't exist".format(user_id))
        get_db().um.delete_organization_membership(organization_id, user_id)
        return "", 204

        
