#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from flask_restx import Namespace, fields, Resource
from flask import request, g, make_response, jsonify

import uuid

from benchsuite.rest.db import get_db

api = Namespace('workloads', description='Workloads operations')


wild = fields.Wildcard(fields.String)

nested_object = api.model('NestedObject', {
    '*': wild
})

_workload_models = []

# the strange thing below is to manage recursive marshalling.. found no better way than this
previous_model = nested_object
workload_model = nested_object
for i in range(1, 3):
    workload_model = api.model('Workload'+str(i), {
        'workload_parameters': fields.Nested(nested_object, skip_none=True),
        'categories': fields.List(fields.String),
        'grants': fields.Nested(nested_object, skip_none=True),
        'categories': fields.List(fields.String),
        'parent_workload': fields.Nested(previous_model, skip_none=True),
        'children_workloads': fields.List(fields.Nested(previous_model, skip_none=True)),
        'abstract': fields.Boolean,
        '*': wild
    })
    previous_model = workload_model

workload_short_model = api.model('WorkloadShort', {
    "name" : fields.String,
    "tool" : fields.String,
    "description" : fields.String
})

@api.route('/download')
class WorkloadListDownload(Resource):

    @api.marshal_with(workload_model, as_list=True, skip_none=True, code=200, description='Returns the list of all workloads')
    def get(self):
        filter = {}
        if request.args.get("workload_name"):
            filter["workload_name"] = request.args.get("workload_name")
        if request.args.get("tool_name"):
            filter["tool_name"] = request.args.get("tool_name")

        flat = ("true" == str(request.args.get('flat')).lower())

        if flat:
            return get_db().wm.list_workloads(filter, requestorName=g.benchUser, embed_parent=False, resolve=True, export=True)
        else:
            return get_db().wm.list_workloads(filter, requestorName=g.benchUser, embed_parent=False, resolve=False, export=True)

@api.route('/')
class WorkloadList(Resource):

    @api.marshal_with(workload_model, as_list=True, skip_none=True, code=200, description='Returns the list of all workloads')
    def get(self):
        filter = {}
        if request.args.get("workload_name"):
            filter["workload_name"] = request.args.get("workload_name")
        if request.args.get("tool_name"):
            filter["tool_name"] = request.args.get("tool_name")

        embed_parent = ("true" == str(request.args.get('embed_parent')).lower())
        resolve = ("true" == str(request.args.get('resolve')).lower())
        return get_db().wm.list_workloads(filter, requestorName=g.benchUser, embed_parent=embed_parent, resolve=resolve)


    @api.expect(workload_model)
    @api.marshal_with(workload_model, code=201, description='Add a new workload')
    def post(self):
        api.payload["id"] = str(uuid.uuid4())
        return get_db().wm.add_workload(api.payload, requestorName=g.benchUser), 201

@api.route('/user_actions')
class AllowedWorkloadsActions(Resource):
    @api.marshal_with(nested_object, as_list=True, code=200, description='Returns the list of available actions on providers')
    def get(self):
        actions = get_db().wm.get_allowed_actions(None, requestorName=g.benchUser)
        return actions

@api.route('/<string:workload_id>/download')
@api.param('workload_id', 'The id of the workload')
class WorkloadDownload(Resource):

    @api.marshal_with(workload_model, as_list=True, skip_none=True, code=200, description='Returns the given workload')
    @api.response(404, description='No such workload')
    def get(self, workload_id):
        flat = ("true" == str(request.args.get('flat')).lower())
        hierarchy = ("true" == str(request.args.get('hierarchy')).lower())
        if hierarchy:
            out = get_db().wm.get_workload_hierarchy_for_export(workload_id, requestorName=g.benchUser)
        else:
            out = [get_db().wm.get_workload(workload_id, requestorName=g.benchUser, embed_parent=False, embed_children=False, resolve=True, export=True)]
        if out==None or len(out)==0 or out[0]==None:
            api.abort(404, "Workload '{}' doesn't exist".format(workload_id))
        else:
            return out

@api.route('/<string:workload_id>')
@api.param('workload_id', 'The id of the workload')
class Workload(Resource):

    @api.marshal_with(workload_model, as_list=False, skip_none=True, code=200, description='Returns the given workload')
    @api.response(404, description='No such workload')
    def get(self, workload_id):
        embed_parent = ("true" == str(request.args.get('embed_parent')).lower())
        embed_children = ("true" == str(request.args.get('embed_children')).lower())
        resolve = ("true" == str(request.args.get('resolve')).lower())
        workload = get_db().wm.get_workload(workload_id, requestorName=g.benchUser, embed_parent=embed_parent, embed_children=embed_children, resolve=resolve)
        if workload==None:
            api.abort(404, "Workload '{}' doesn't exist".format(workload_id))
        else:
            return workload

    @api.response(204, description='Deletes a workload')
    @api.response(404, description='No such workload')
    def delete(self, workload_id):
        ret = get_db().wm.delete_workload(workload_id, requestorName=g.benchUser)
        if ret and ret=="unauthorized":
            api.abort(401, "Not authorized to remove workload {}".format(workload_id))
        if ret and ret.deleted_count>0:
            return '', 204
        else:
            api.abort(404, "Workload {} doesn't exist".format(workload_id))

    @api.expect(workload_model)
    @api.marshal_with(workload_model, code=200, description='Update an existing workload')
    def put(self, workload_id):
        if not get_db().wm.workload_exists(workload_id):
            api.abort(404, "Workload '{}' doesn't exist".format(workload_id))
        api.payload["id"] = workload_id
        ret = get_db().wm.update_workload(api.payload, requestorName=g.benchUser)

        if ret and ret=="unauthorized":
            api.abort(401, "Not authorized to updated workload {}".format(workload_id))
        else:
            return '', 204

@api.route('/<string:workload_id>/user_actions')
@api.param('workload_id', 'The id of the workload')
class AllowedWorkloadActions(Resource):
    @api.marshal_with(nested_object, as_list=True, code=200, description='Returns the list of available actions on the workload')
    @api.response(404, description='No such workload')
    def get(self, workload_id):
        actions = get_db().wm.get_allowed_actions(workload_id, requestorName=g.benchUser)
        return actions
