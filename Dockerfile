#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

# this image uses multi-stage build to avoid to keep in the final image the
# tools used to build some dependency (e.g. gcc, git).
# See here for more info: https://pythonspeed.com/articles/multi-stage-docker-python/

FROM python:3.9-slim as compile-image

MAINTAINER Gabriele Giammatteo <gabriele.giammatteo@eng.it>

RUN apt-get update
RUN apt-get -y install git gcc

RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"


COPY dist /dist

ARG CI_COMMIT_BRANCH
ENV PIP_OPTS=${CI_COMMIT_BRANCH:+--pre}

# install the benchsuite.rest from the build
RUN pip install ${PIP_OPTS} --no-cache-dir --extra-index-url https://gitlab.res.eng.it/api/v4/groups/89/-/packages/pypi/simple /dist/benchsuite.rest-*.whl

# install also benchsuite.stdlib which is not a dependency of the rest, but it is needed for the /provider/{:id}/check operation
RUN pip install ${PIP_OPTS} --no-cache-dir --extra-index-url https://gitlab.res.eng.it/api/v4/groups/89/-/packages/pypi/simple benchsuite.stdlib


FROM python:3.9-slim AS build-image
COPY --from=compile-image /opt/venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

# adds continuous integration commit reference
ARG CI_COMMIT_REF_NAME
ARG CI_COMMIT_SHA
RUN echo "$CI_COMMIT_SHA@$CI_COMMIT_REF_NAME" > /ci_build_ref
ENV BUILD_REF_NAME $CI_COMMIT_REF_NAME
ENV BUILD_COMMIT $CI_COMMIT_SHA

EXPOSE 5000

ENV BENCHSUITE_CONFIG_FOLDER /

ENTRYPOINT ["bsrest"]
