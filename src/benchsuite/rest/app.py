#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import argparse
import logging
import signal
import sys
import json

import cherrypy

import os

import click
from flask import Flask
from flask_restx import Swagger

from flask import request, g

from benchsuite.core.cli.cli_config import options_from_model, options_from_model_2
from benchsuite.core.config import BaseBenchsuiteConfig
from benchsuite.rest.apiv1 import blueprint as blueprint1
from benchsuite.rest.apiv2 import blueprint as blueprint2
from benchsuite.rest.apiv3 import blueprint as blueprint3
from benchsuite.rest.apiv3 import api as apiv3

from benchsuite.rest.config import Config
from benchsuite.rest.db import create_global_db_instance, create_global_um_instance

from benchsuite.core.util.version import container_version, modules_version, fmt_versions

app = Flask(__name__)


config_instance: Config = None

#app.config.SWAGGER_UI_JSONEDITOR = True
app.config.SWAGGER_UI_DOC_EXPANSION = 'list'

# To avoid suggestions in 404 replies
app.config['ERROR_404_HELP'] = False

app.config['PROPAGATE_EXCEPTIONS'] = False

app.register_blueprint(blueprint1)
app.register_blueprint(blueprint2)
app.register_blueprint(blueprint3)

def on_exit(sig, func=None):
    print('Bye bye...')
    sys.exit(1)

# extract the requestor identity, if any
@app.before_request
def before_request():
    g.config = config_instance
    user = request.headers.get('benchsuite-user')
    if user:
        g.benchUser = user
    else:
        g.benchUser = None
    org = request.headers.get('benchsuite-user-org')
    if org:
        g.benchUserOrg = org
    else:
        g.benchUserOrg = None

def generate_swagger_specs():
    app.config['SERVER_NAME'] = 'example.org:80'
    with app.app_context():
        return json.dumps(Swagger(apiv3).as_dict(), indent=2)


@click.command()
@options_from_model(Config)
@options_from_model_2(Config)
@click.pass_context
# ------------------------------
def cli(ctx, config, **kwargs):

    print(fmt_versions())
    print(f'\nConfig: {config.dict()}\n')
    global config_instance
    config_instance = config

    create_global_db_instance(**config.flatten_dict())
    create_global_um_instance(**config.flatten_dict())

    signal.signal(signal.SIGINT, on_exit)

    logging.basicConfig(
        level=logging.DEBUG,
        stream=sys.stdout,
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    host = '0.0.0.0'
    port = 5005

    cherrypy.tree.graft(app.wsgi_app, '/')
    cherrypy.config.update({'server.socket_host': host,
                        'server.socket_port': port,
                        'engine.autoreload.on': True,
                        })

    cherrypy.engine.start()
    cherrypy.engine.block()


def main(args=None):
    cli(auto_envvar_prefix='BS', args=args)


if __name__ == '__main__':
    main(args=sys.argv)
