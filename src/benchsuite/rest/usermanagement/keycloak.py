#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import time
import traceback

from keycloak import KeycloakAdmin
from keycloak.exceptions import KeycloakAuthenticationError

logger = logging.getLogger(__name__)


class KeycloakClient:

    def __init__(self, oauth2_server, oauth2_realm, oauth2_rest_client, oauth2_rest_secret, **kwargs):

        self.server = oauth2_server
        self.realm = oauth2_realm
        self.client = oauth2_rest_client
        self.secret = oauth2_rest_secret
        self._client = None
        self._token_issue_time = None

    def get_client(self):

        # if token is expired, re-init the client
        # NOTE: KeycloakAdmin has an auto_token_refresh feature, but a better investigation
        # on how to make it work is needed
        if self._client:
            token_age = time.time() - self._token_issue_time
            if token_age > self._client.token['expires_in']:
                logger.debug('Token expired %s seconds ago. Re-authenticating...', int(token_age - self._client.token['expires_in']))
                self._client = None

        if not self._client:
            self._client = KeycloakAdmin(
                server_url=self.server,
                realm_name=self.realm,
                client_id=self.client,
                client_secret_key=self.secret,
                verify=False
            )
            logger.debug('Authenticated. Token expires in %s seconds', self._client.token['expires_in'])
            self._token_issue_time = time.time()
        return self._client

    def get_users(self):
        try:
            return self.get_client().get_users()
        except Exception as e:
            print(traceback.format_exc())
            return None

    def get_user(self, username):
        try:
            return self.get_client().get_user(self.get_client().get_user_id(username))
        except Exception as e:
            print(traceback.format_exc())
            return None

    def get_user_groups(self, username):
        try:
            return self.get_client().get_user_groups(self.get_client().get_user_id(username))
        except Exception as e:
            print(traceback.format_exc())
            return None
        
    def user_exists(self, username):
        return self.get_user(username) is not None
