#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from flask_restx import Namespace, fields, Resource
from flask import request, g


import uuid

from benchsuite.rest.db import get_db

api = Namespace('executions', description='Executions operations')


wild = fields.Wildcard(fields.String(default=""))

nested_object = api.model('NestedObject', {
    '*': wild
})

metric_body_model = api.model("MetricBody", {
    "value": fields.Float,
    "unit": fields.String
})

wild2 = fields.Wildcard(fields.Nested(metric_body_model))

metrics_model = api.model("Metric", {
    '*': wild2
})

test_model = api.model("Test", {
    "description": fields.String,
    "categories": fields.List(fields.String),
    '*': wild    
})

vm_model = api.model("VM", {
    "platform": fields.String,
    "*": wild    
})

provider_model = api.model("Provider", {
    "*": wild    
})

environment_model = api.model("Environment", {
    "type": fields.String,
    "vms": fields.List(fields.Nested(vm_model)),
    '*': wild    
})

property_model = api.model("Property", {
    "*": wild
})


log_model = api.model("Log", {
    "vm": fields.String,
    "stdout": fields.String,
    "stderr": fields.String,
    "*":wild
    
})

run_execution_model = api.model("run_execution_model", {
    "logs": fields.List(fields.Nested(log_model, skip_none=True)),
    "environment": fields.Nested(environment_model, skip_none=True),
    '*': wild    
})

run_component_info_model = api.model("run_component_info_model", {
    "stdout": fields.String,
    "stderr": fields.String,
    "node": fields.String,
    "runtime": fields.Float,
    "retval": fields.Integer
})

run_status_steps_components_model = api.model("RunStatusStepsComponents", {
    '*': fields.Wildcard(fields.Nested(run_component_info_model, skip_none=True))
})


step_status_model = api.model("step_status_model", {
    "logs": fields.List(fields.String, skip_none=True),
    "start": fields.Float,
    "end": fields.Float,
    "errno": fields.Integer,
    "exception": fields.String,
    "traceback": fields.String,
    "components": fields.Nested(run_status_steps_components_model, skip_none=True)
})

wild3 = fields.Wildcard(fields.Nested(run_component_info_model))

executor_components_info_model = api.model("ExecutorInfo", {
    "*": wild3
})

executor_info_model = api.model("ExecutorInfo", {
    "sut_nodes": fields.List(fields.String)
})

run_status_steps_model = api.model("RunStatusSteps", {
    '*': fields.Wildcard(fields.Nested(step_status_model, skip_none=True))
})

run_status_model = api.model("RunStatus", {
    "sut_nodes": fields.List(fields.String),
    "steps": fields.Nested(run_status_steps_model, skip_none=True),
    "metrics": fields.Nested(metrics_model, skip_none=True),
    "last_executed_step": fields.String
})


exception_data = api.model("ExceptionData", {
    "headers": fields.Nested(property_model, skip_none=True),
    "logs": fields.List(fields.Nested(log_model, skip_none=True)),
    '*': wild        
})

run_model_results = api.model('RunResults', {
    "workload" : fields.Nested(test_model, skip_none=True),
    "provider": fields.Nested(provider_model, skip_none=True),
    'status': fields.Nested(run_status_model, skip_none=True),
    "properties": fields.Nested(property_model, skip_none=True)
})

error_model = api.model("ErrorModel", {
    "test" : fields.Nested(test_model, skip_none=True),
    "provider": fields.Nested(provider_model, skip_none=True),
    'status': fields.Nested(run_status_model, skip_none=True),
    "properties": fields.Nested(property_model, skip_none=True),
    "starttime": fields.String,
    "metrics": fields.Nested(metrics_model, skip_none=True),
    "*": wild
})

run_model = api.model('Run', {
    "results" : fields.Nested(run_model_results, skip_none=True),
    "errors" : fields.Nested(run_model_results, skip_none=True),
    '*': wild    
})

from benchsuite.rest.apiv3.schedules import schedule_model
from benchsuite.rest.apiv3.schedules import schedule_model_short
from benchsuite.rest.apiv3.providers import provider_model
from benchsuite.rest.apiv3.providers import provider_model_short

runs_status_model = api.model('RunsStatus', {
    "ok" : fields.List(fields.String),
    "error" : fields.List(fields.String)
})

execution_model_short = api.model('ExecutionShort', {
    "_id" : fields.String,
    "status" : fields.String,
    "time" : fields.String,
    'runs_status': fields.Nested(runs_status_model, skip_none=True),
    "starttime" : fields.String,
    "endtime" : fields.String,
    "id":fields.String,
    "one-time": fields.Boolean,
    'schedule': fields.Nested(schedule_model_short, skip_none=True),
    'provider': fields.Nested(provider_model_short, skip_none=True)
})

execution_model = api.model('Execution', {
    "_id" : fields.String,
    "executor" : fields.String,
    "status" : fields.String,
    "time" : fields.String,
    "starttime" : fields.String,
    "endtime" : fields.String,
    'runs_status': fields.Nested(runs_status_model, skip_none=True),
    "ap_job_id" : fields.String,
    "schema_ver" : fields.String,
    "schedule_id" : fields.String,
    "error" : fields.String,
    "error_code" : fields.String,
    "error_log" : fields.String,
    "error_traceback" : fields.String,
    "exception" : fields.String,
    "docker_exception" : fields.String,
    "one-time": fields.Boolean,
    "id":fields.String,
    'schedule': fields.Nested(schedule_model, skip_none=True),
    'provider': fields.Nested(provider_model, skip_none=True),
    'log': fields.String
})

@api.route('/')
class ExecutionList(Resource):

    @api.marshal_with(execution_model_short, as_list=True, code=200, skip_none=True, description='Returns the list of all executions')
    def get(self):
        return get_db().em.list_executions(requestorName=g.benchUser, requestorOrg=g.benchUserOrg)

@api.route('/user_actions')
class AllowedExecutionsActions(Resource):
    @api.marshal_with(nested_object, as_list=True, code=200, description='Returns the list of available actions on executions')
    def get(self):
        actions = get_db().em.get_allowed_actions(None, requestorName=g.benchUser)
        return actions

@api.route('/<string:execution_id>')
@api.param('execution_id', 'The id of the execution')
class Execution(Resource):

    @api.marshal_with(execution_model, as_list=False, code=200, skip_none=True, description='Returns the given execution')
    @api.response(404, description='No such execution')
    def get(self, execution_id):
        execution = get_db().em.get_execution(execution_id, requestorName=g.benchUser, requestorOrg=g.benchUserOrg)
        if execution==None:
            api.abort(404, "Execution '{}' doesn't exist".format(execution_id))
        else:
            return execution

@api.route('/<string:execution_id>/request')
@api.param('execution_id', 'The id of the execution')
class ExecutionRequest(Resource):

    @api.marshal_with(schedule_model, as_list=False, code=200, skip_none=True, description='Returns the schedule generating this execution')
    @api.response(404, description='No such execution')
    def get(self, execution_id):
        request = get_db().em.get_request_for_execution(execution_id, requestorName=g.benchUser, requestorOrg=g.benchUserOrg)
        if request==None:
            api.abort(404, "Execution '{}' doesn't exist, or not enough rights to get the corresponding request".format(execution_id))
        else:
            return request
                

@api.route('/<string:execution_id>/runs')
@api.param('execution_id', 'The id of the execution')
class RunResults(Resource):

    @api.marshal_with(run_model_results, as_list=True, code=200, skip_none=True, description='Returns the given execution')
    @api.response(404, description='No such execution')
    def get(self, execution_id):
        execution = get_db().em.get_execution(execution_id, requestorName=g.benchUser, requestorOrg=g.benchUserOrg)
        if execution==None:
            api.abort(404, "Execution '{}' doesn't exist".format(execution_id))
        else:
            return get_db().em.list_runs(execution_id, requestorName=g.benchUser, requestorOrg=g.benchUserOrg)

