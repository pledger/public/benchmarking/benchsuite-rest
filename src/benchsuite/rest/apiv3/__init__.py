#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from flask import Blueprint
from flask_restx import Api

from benchsuite.rest.apiv1 import description
from benchsuite.rest.apiv1 import api as apiv1
from benchsuite.rest.apiv2 import api as apiv2

blueprint = Blueprint('apiv3', __name__, url_prefix='/api/v3')

api = Api(
    blueprint,
    title='Benchmarking Suite REST API',
    version='3.0',
    description=description,
    # All API metadatas
)

# reuse the same error handlers of apiv1
from benchsuite.rest.apiv1.errors import register_error_handlers

register_error_handlers(api)

from benchsuite.rest.apiv1.sessions import api as sessions_ns
#from benchsuite.rest.apiv1.providers import api as providers_ns
from benchsuite.rest.apiv1.benchmarks import api as benchmarks_ns

#from benchsuite.rest.apiv2.executions import api as executions_ns

from .users import api as users_ns
from .organizations import api as organizations_ns
from .providers import api as providers_ns
from .workloads import api as workloads_ns
from .schedules import api as schedules_ns
from .executions import api as executions_ns

api.add_namespace(sessions_ns)
api.add_namespace(executions_ns)
api.add_namespace(providers_ns)
api.add_namespace(benchmarks_ns)
api.add_namespace(users_ns)
api.add_namespace(organizations_ns)
api.add_namespace(providers_ns)
api.add_namespace(workloads_ns)
api.add_namespace(schedules_ns)
api.add_namespace(executions_ns)

